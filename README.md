# Travel Advisor

This is a project i did following a course by Javascript Mastery Youtube channel

## The main purpose of this is to refresh my knowledge of React.js and to learn new definitions.

### Some of the React themes in this lesson are:

- Render method
- Main App component
- Components & props
- Lambda functions
- Empty components
- React useState and useEffect
- Import & export
- API usage
- Components dynimacally populated


### Some of the libraries used are
- Material UI
- React google maps
- Axios

Greetings...